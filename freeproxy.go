package freeproxy

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"golang.org/x/net/html"
	xmlpath "gopkg.in/xmlpath.v2"
)

type FreeProxy struct {
	IPAddress   string
	Port        string
	Code        string
	Country     string
	Anonymity   string
	Google      string
	HTTPS       string
	LastChecked string
	Timeout     time.Duration
}

type CheckIP struct {
	IP string
}

func NewFreeProxy() *FreeProxy {
	return &FreeProxy{Timeout: 1}
}

func (f *FreeProxy) Check() bool {
	req, err := http.NewRequest("GET", "http://api.ipify.org/?format=json", nil)
	if err != nil {
		return false
	}
	proxyURL, err := url.Parse(fmt.Sprintf("http://%s:%s", f.IPAddress, f.Port))
	if err != nil {
		return false
	}
	client := &http.Client{
		Timeout: time.Duration(f.Timeout * time.Second),
		Transport: &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			Proxy: http.ProxyURL(proxyURL)},
	}
	req.Close = true
	resp, err := client.Do(req)
	if err != nil {
		return false
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false
	}
	var checkip CheckIP
	err = json.Unmarshal(body, &checkip)
	if err != nil {
		return false
	}
	return checkip.IP == f.IPAddress
}

func (f *FreeProxy) IsEliteProxy() bool {
	if f.Anonymity == "elite proxy" {
		return true
	}
	return false
}

func (f *FreeProxy) IsAnonymous() bool {
	if f.Anonymity == "anonymous" {
		return true
	}
	return false
}

func (f *FreeProxy) IsTransparent() bool {
	if f.Anonymity == "transparent" {
		return true
	}
	return false
}

func (f *FreeProxy) IsHttps() bool {
	if f.HTTPS == "yes" {
		return true
	}
	return false
}

func (f *FreeProxy) Set(key int, value string) {
	switch key {
	case 0:
		f.IPAddress = value
	case 1:
		f.Port = value
	case 2:
		f.Code = value
	case 3:
		f.Country = value
	case 4:
		f.Anonymity = value
	case 5:
		f.Google = value
	case 6:
		f.HTTPS = value
	case 7:
		f.LastChecked = value
	}
}

type ProxyGenerator struct {
	list    []*FreeProxy
	counter int
}

func NewProxyGenerator() *ProxyGenerator {
	return &ProxyGenerator{}
}

func (pg *ProxyGenerator) incr() {
	pg.counter += 1
}

func (pg *ProxyGenerator) load() error {
	req, err := http.NewRequest("GET", "https://free-proxy-list.net", nil)
	if err != nil {
		return err
	}
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	req.Close = true
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	reader := strings.NewReader(string(body))
	root, err := html.Parse(reader)

	if err != nil {
		return err
	}

	var b bytes.Buffer
	html.Render(&b, root)
	fixedHTML := b.String()

	reader = strings.NewReader(fixedHTML)
	xmlroot, xmlerr := xmlpath.ParseHTML(reader)

	if xmlerr != nil {
		return xmlerr
	}

	path := xmlpath.MustCompile(`//td`)

	it := path.Iter(xmlroot)

	incr := 0

	freeproxy := NewFreeProxy()
	pg.list = []*FreeProxy{}
	for it.Next() {
		if 8 <= incr {
			pg.list = append(pg.list, freeproxy)
			freeproxy = NewFreeProxy()
			incr = 0
		}
		freeproxy.Set(incr, it.Node().String())
		incr++
		if len(pg.list) >= 50 {
			break
		}
	}
	return nil
}

var mutex = &sync.Mutex{}

func (pg *ProxyGenerator) Get() (*FreeProxy, error) {
	mutex.Lock()
	defer pg.incr()

	if len(pg.list) > pg.counter {
		mutex.Unlock()
		return pg.list[pg.counter], nil
	}
	pg.counter = 0
	err := pg.load()
	if err != nil {
		mutex.Unlock()
		return nil, err
	}
	mutex.Unlock()
	return pg.Get()
}
